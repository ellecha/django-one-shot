from django import forms
from .models import TodoList, TodoItem


class TodoListForm(forms.ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(forms.ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]


# from django.forms import ModelForm
# from todos.models import TodoItem, TodoList


# class TodoListForm(ModelForm):
#     class Meta:
#         model = TodoList
#         fields = "__all__"


# class TodoItemForm(ModelForm):
#     class Meta:
#         model = TodoItem
#         fields = "__all__"
