from django.shortcuts import render, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm, TodoItemForm

# Create your views here.


def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {"todo_lists": todo_lists}
    return render(request, "todos/list.html", context)


# context will be the django name


def todo_list_detail(request, id):
    todo_list = TodoList.objects.get(id=id)
    context = {"todo_list": todo_list}
    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()

    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {"form": form}

    return render(request, "todos/update.html", context)


def todo_list_delete(request, id):
    if request.method == "POST":
        todo_list = TodoList.objects.get(id=id)
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {"form": form}
    return render(request, "todos/todo_item_create.html", context)


def todo_item_update(request, id):
    todo_item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todo_item.list.id)

    else:
        form = TodoItemForm(instance=todo_item)

    context = {"form": form}

    return render(request, "todos/todo_item_update.html", context)
